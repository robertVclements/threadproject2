﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExpertsLibrary
{
    public class Package_Product_Supplier
    {
        public Package_Product_Supplier() { }
        public int PackageID { get; set; }
        public int ProductSupplierID { get; set; }
    }
}
