﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExpertsLibrary
{
    public class Product_Supplier
    {
        public Product_Supplier() { }

        public int ProductSupplierID { get; set; }
        public int ProductID { get; set; }
        public int SupplierID { get; set; }

        public Product_Supplier copyproduct_supplier()
        {
            Product_Supplier copy = new Product_Supplier();
            copy.ProductSupplierID = ProductSupplierID;
            copy.ProductID = ProductID;
            copy.SupplierID = SupplierID;
            return copy;
        }
    }
}
