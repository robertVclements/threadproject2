﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExpertsLibrary
{
    public static class Product_SupplierDB
    {
        /// <summary>
        /// the function is used to get the data from the Products_Suppliers table
        /// </summary>
        /// <returns></returns>
        public static List<Product_Supplier> GetAllProduct_Supplier()
        {
            List<Product_Supplier> product_suppliers = new List<Product_Supplier>();
            Product_Supplier ps = null;
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT * " +
                                     "FROM Products_Suppliers " +
                                     "ORDER BY ProductSupplierID";
            SqlCommand cmd = new SqlCommand(selectStatement, con);

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) // while there are orders
                {
                    ps = new Product_Supplier();
                    ps.ProductSupplierID = (int)reader["ProductSupplierID"];
                    ps.ProductID = (int)reader["ProductID"];
                    ps.SupplierID = (int)reader["SupplierID"];
                    product_suppliers.Add(ps);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return product_suppliers;
        }

        /// <summary>
        /// the function is used to update the data in the Products_Suppliers Table
        /// </summary>
        /// <param name="oldProduct_Supplier"></param>
        /// <param name="newProduct_Supplier"></param>
        /// <returns></returns>
        public static bool UpdateProduct_Supplier(Product_Supplier oldProduct_Supplier, Product_Supplier newProduct_Supplier)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string updateStatement =    "update Products_Suppliers " +
                                        "set ProductID = @newProductID, " +
                                        "SupplierID = @newSupplierID " +
                                        "where ProductSupplierID = @oldProductSupplierID " +
                                        "and ProductID = @oldProductID " +
                                        "and SupplierID = @oldSupplierID ";
            SqlCommand cmd = new SqlCommand(updateStatement, con);
            cmd.Parameters.AddWithValue("@newProductID", newProduct_Supplier.ProductID);
            cmd.Parameters.AddWithValue("@newSupplierID", newProduct_Supplier.SupplierID);

            cmd.Parameters.AddWithValue("@oldProductSupplierID", oldProduct_Supplier.ProductSupplierID);
            cmd.Parameters.AddWithValue("@oldProductID", oldProduct_Supplier.ProductID);
            cmd.Parameters.AddWithValue("@oldSupplierID", oldProduct_Supplier.SupplierID);
            try
            {
                con.Open();
                int count = cmd.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }


        /// <summary>
        /// function is used to delete the data from Product_supplier table
        /// </summary>
        /// <param name="PS"></param>
        /// <returns></returns>
        public static bool deleteProduct_Supplier(Product_Supplier PS)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string deleteStatement = "delete Products_Suppliers from Products_Suppliers " +
                                        "left join Products on Products_Suppliers.ProductId = Products.ProductId "+
                                        "LEFT join Suppliers on Products_Suppliers.SupplierID = Suppliers.SupplierID "+
                                        "where ProductSupplierId = @ProductSupplierId " +
                                        "and ProductID = @ProductID " +
                                        "and SupplierID = @SupplierID";
            SqlCommand cmd = new SqlCommand(deleteStatement, con);
            cmd.Parameters.AddWithValue("@ProductSupplierID", PS.ProductSupplierID);
            cmd.Parameters.AddWithValue("@ProductID", PS.ProductID);
            cmd.Parameters.AddWithValue("@SupplierID", PS.SupplierID);
            try
            {
                con.Open();
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// function is used to add data to Product_Supplier table
        /// </summary>
        /// <param name="PS"></param>
        /// <returns></returns>
        public static int AddProduct_Supplier(Product_Supplier PS)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string insertStatement = "insert into Product_Suppliers (ProductSuppleirID, ProductID, SupplierID) " +
                                        "values(@ProductSupplierID, @ProductID, @SupplierID)";
            SqlCommand cmd = new SqlCommand(insertStatement, con);
            cmd.Parameters.AddWithValue("@ProductSupplierID", PS.ProductSupplierID);
            cmd.Parameters.AddWithValue("@ProductID", PS.ProductID);
            cmd.Parameters.AddWithValue("@SupplierID", PS.SupplierID);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                String selectQuery = "select ident_current('Product_Suppliers') from Product_Suppliers";
                SqlCommand selectCmd = new SqlCommand(selectQuery, con);
                int ProductSupplierID = Convert.ToInt32(selectCmd.ExecuteScalar());
                return ProductSupplierID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
