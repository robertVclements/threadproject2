﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TravelExpertsLibrary;

namespace TravelExpertsAPP
{
    public partial class TravelExpertsForm : Form
    {
        List<Product> products;
        private Product product;
        
        List<Supplier> suppliers; //empty list       
        private Supplier supplier;
       
        List<Product_Supplier> product_suppliers = Product_SupplierDB.GetAllProduct_Supplier();
        Product_Supplier oldproduct_supplier;
        int rowindex;

        public TravelExpertsForm()
        {
            InitializeComponent();
        }

        private void TravelExpertsForm_Load(object sender, EventArgs e)
        {
            DisplayProducts(); //calling method for displaying products
            DisplaySupplier();
        }
        /// <summary>
        /// Author: Robert Clements
        /// Purpose: displays all of the products from database in data grid view
        /// </summary>
        private void DisplayProducts()
        {
            products = ProductDB.GetAllProduct();
            productDataGridView.DataSource = products;
            
        }

        /// <summary>
        /// Author: Robert Clements
        /// Purpose: adds product to database when user clicks button 
        ///             then returns to the main form and reloads product data grid view
        /// </summary>
        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            AddModifyProduct addModifyProducts = new AddModifyProduct(); // make new form
            addModifyProducts.addProdcut = true; //setting value to true to show add button was clicked
            DialogResult result = addModifyProducts.ShowDialog();
            if (result == DialogResult.OK) //if accept button was clicked
            {
                product = addModifyProducts.product;
                DisplayProducts();
            }
        }

        /// <summary>
        /// Author: Robert Clements
        /// Purpose: When user clicks delete button it will delete selected item in the data grid view
        /// </summary>
        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            SelectedRowProduct();

            if (result == DialogResult.Yes)
            {
                try
                {
                    if (!ProductDB.DeleteProduct(product))
                    {
                        MessageBox.Show("Another user has updated or deleted product" + product.ProdName, "Datebase Error");
                    }
                    DisplayProducts();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void SelectedRowProduct()
        {
            product = new Product();
            int index = productDataGridView.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = productDataGridView.Rows[index];
            product.ProductID = Convert.ToInt32(selectedRow.Cells[0].Value);
            product.ProdName = selectedRow.Cells[1].Value.ToString();
        }

        private void btnModifyProduct_Click(object sender, EventArgs e)
        {
            SelectedRowProduct();
            AddModifyProduct modifyProduct = new AddModifyProduct();
            modifyProduct.addProdcut = false;
            modifyProduct.product = product;
            DialogResult result = modifyProduct.ShowDialog();
            DisplayProducts();
            
        }
      
        /**************************************************************************************************************
        * Author : Sneha Patel(000783907)
        * Date : 27th July, 2018
        * Purpose: Add, Modify and Delete Buttons Events
        **************************************************************************************************************/

        private void DisplaySupplier()
        {
            suppliers = SupplierDB.GetAllSuppliers();
            supplierDataGridView.DataSource = suppliers;         
        }

        private void SelectedRowSupplier()
        {
            supplier = new Supplier();
            int index = supplierDataGridView.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = supplierDataGridView.Rows[index];
            supplier.SupplierId = Convert.ToInt32(selectedRow.Cells[0].Value);
            supplier.SupName = selectedRow.Cells[1].Value.ToString();
        }
         
        // Supplier's Add Button
        private void btnAddSupplier_Click(object sender, EventArgs e)
        {
            frmSupplierAddModify addSupplierForm = new frmSupplierAddModify();
            addSupplierForm.addSupplier = true;
            DialogResult result = addSupplierForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                supplier = addSupplierForm.supplier;
                DisplaySupplier();
            }
        }

        // Supplier's Modify Button
        private void btnModifySupplier_Click(object sender, EventArgs e)
        {
            SelectedRowSupplier();
            frmSupplierAddModify modifySupplierForm = new frmSupplierAddModify();
            modifySupplierForm.addSupplier = false;
            modifySupplierForm.supplier = supplier;
            DialogResult result = modifySupplierForm.ShowDialog();
            DisplaySupplier();
        }

        // Supplier's Delete Button
        private void btnDeleteSupplier_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("DO you really want to delete this Supplier?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            SelectedRowSupplier();

            if(result == DialogResult.Yes)
            {
                try
                {
                    if (!SupplierDB.DeleteSupplier(supplier))
                    {
                        MessageBox.Show("Another user has updated or deleted product" + supplier.SupName, "Datebase Error");
                    }
                    DisplaySupplier();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }
        /**********************************************Supplier's Session End*********************************************/

        private void TravelExpertsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'travelExpertsDataSet.Products_Suppliers' table. You can move, or remove it, as needed.
            this.products_SuppliersTableAdapter.Fill(this.travelExpertsDataSet.Products_Suppliers);

        }

        private void products_SuppliersDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            rowindex = products_SuppliersDataGridView.CurrentCell.RowIndex;
        }
       
        private void btnModifyProductSupplier_Click(object sender, EventArgs e)
        {
            
            try
            {
                oldproduct_supplier = product_suppliers[rowindex].copyproduct_supplier(); // make a  separate copy before update
                formUpdateProduct_Supplier updateForm = new formUpdateProduct_Supplier();
                updateForm.newproduct_Supplier = product_suppliers[rowindex]; // "pass" current customer to the form
                updateForm.oldProduct_Supplier = oldproduct_supplier;        // same for the original order data
                DialogResult result = updateForm.ShowDialog(); // display model second form
                if (result == DialogResult.OK) // update accepted
                {
                    // refresh the grid view
                    CurrencyManager cm = (CurrencyManager)products_SuppliersDataGridView.BindingContext[product_suppliers];
                    cm.Refresh();
                }
                else // update cancelled
                {
                    product_suppliers[rowindex] = oldproduct_supplier; // revert to the old values
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, GetType().ToString());
                //throw ex;
            }
            this.products_SuppliersTableAdapter.Fill(this.travelExpertsDataSet.Products_Suppliers);
        }

       

        private void btnDeleteProductSupplier_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Delete " + product_suppliers[rowindex].ProductSupplierID + "?",
                "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {

                    if (!Product_SupplierDB.deleteProduct_Supplier(product_suppliers[rowindex]))
                    {
                        MessageBox.Show("Another user has updated or deleted " +
                        "that customer.", "Database Error");
                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        
        
        private void btnAddProductSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                formAddProducts_Suppliers addForm = new formAddProducts_Suppliers();
                addForm.product_Supplier = product_suppliers[rowindex];

                DialogResult result = addForm.ShowDialog(); // display model second form
                if (result == DialogResult.OK) // update accepted
                {
                    // refresh the grid view
                    CurrencyManager cm = (CurrencyManager)products_SuppliersDataGridView.BindingContext[product_suppliers];
                    cm.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, GetType().ToString());
                //throw ex;
            }
            this.products_SuppliersTableAdapter.Fill(this.travelExpertsDataSet.Products_Suppliers);
        }
    }
}
