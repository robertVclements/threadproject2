﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExpertsLibrary;


namespace TravelExpertsAPP
{
    public partial class formUpdateProduct_Supplier : Form
    {
        public Product_Supplier newproduct_Supplier;
        public Product_Supplier oldProduct_Supplier;

        public formUpdateProduct_Supplier()
        {
            InitializeComponent();
        }

        private void products_SuppliersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.products_SuppliersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.travelExpertsDataSet);

        }

        private void products_SuppliersBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.products_SuppliersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.travelExpertsDataSet);

        }

        private void formUpdateProduct_Supplier_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'travelExpertsDataSet.Products_Suppliers' table. You can move, or remove it, as needed.
            this.products_SuppliersTableAdapter.Fill(this.travelExpertsDataSet.Products_Suppliers);
            List<Product_Supplier> orders = Product_SupplierDB.GetAllProduct_Supplier();

            txtProductSupplierID.Text = oldProduct_Supplier.ProductSupplierID.ToString();
            txtProductID.Text = Convert.ToString(oldProduct_Supplier.ProductID);
            txtSupplierID.Text = Convert.ToString(oldProduct_Supplier.SupplierID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // set dialog result to Retry
            this.DialogResult = DialogResult.Retry;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            newproduct_Supplier.ProductSupplierID = Convert.ToInt32(txtProductSupplierID.Text);
            newproduct_Supplier.ProductID = Convert.ToInt32(txtProductID.Text);
            newproduct_Supplier.SupplierID = Convert.ToInt32(txtSupplierID.Text);
            bool success = Product_SupplierDB.UpdateProduct_Supplier(oldProduct_Supplier, newproduct_Supplier);
            if (success)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.Retry;
            }

        }
    }
}
